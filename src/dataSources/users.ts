import { User } from "../models/user";

export interface UserDataSource {
  findById(id: string): Promise<User | null>;
}
