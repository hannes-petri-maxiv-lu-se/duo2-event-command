import { Proposal } from "../models/proposal";

export interface ProposalDataSource {
  findById(id: string): Promise<Proposal | null>;
}
