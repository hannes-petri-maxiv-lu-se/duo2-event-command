import { CommandContext, DataSources } from "./app";

export interface CommandRejection extends Event {
  type: "COMMAND_REJECTION";
  subtype: string;
}

function isRejection<EventT extends Event>(
  event: EventT | CommandRejection
): event is CommandRejection {
  return event.type === "COMMAND_REJECTION";
}

export type CommandHandlerResult<EventT> =
  | EventT
  | CommandRejection
  | undefined;

export type AsyncCommandHandlerResult<EventT> = Promise<
  CommandHandlerResult<EventT>
>;

export type EventHandlerResult = PipelineResult | undefined;
export type AsyncEventHandlerResult = Promise<EventHandlerResult>;

type CommandHandler<CommandT, EventT> = (
  command: CommandT,
  context: CommandContext,
  dataSources: DataSources
) => Promise<CommandHandlerResult<EventT>>;

type EventHandler<EventT> = (event: EventT) => Promise<EventHandlerResult>;

type PipelineError =
  | "COMMAND_UNHANDLED"
  | "COMMAND_REJECTED"
  | "EVENT_UNHANDLED"
  | "EVENT_ERROR";

export type PipelineResult =
  | { success: true; id: string }
  | {
      success: false;
      error: PipelineError;
      message: string;
    };

type Pipeline<CommandT> = (
  command: CommandT,
  context: CommandContext
) => Promise<PipelineResult>;

interface Event {
  type: string;
}

interface Command {
  type: string;
}

export function commandRejection(subtype: string): CommandRejection {
  return { type: "COMMAND_REJECTION", subtype };
}

export function buildPipeline<CommandT extends Command, EventT extends Event>(
  commandHandler: CommandHandler<CommandT, EventT>,
  eventHandler: EventHandler<EventT>,
  dataSources: DataSources
): Pipeline<CommandT> {
  return async function(
    command: CommandT,
    context: CommandContext
  ): Promise<PipelineResult> {
    const event = await commandHandler(command, context, dataSources);
    if (event == null) {
      return {
        success: false,
        error: "COMMAND_UNHANDLED",
        message: `The command ${command.type} was not handled`
      };
    }

    if (isRejection(event)) {
      return {
        success: false,
        error: "COMMAND_REJECTED",
        message: `The command ${command.type} was rejected for reason: ${
          event.subtype
        }`
      };
    }

    let result: EventHandlerResult;
    try {
      result = await eventHandler(event);
    } catch (err) {
      return {
        success: false,
        error: "EVENT_ERROR",
        message: `The event ${event.type} failed due to: ${err.message}`
      };
    }

    if (result == null) {
      return {
        success: false,
        error: "EVENT_UNHANDLED",
        message: `The event ${event.type} was not handled`
      };
    }

    return result;
  };
}
