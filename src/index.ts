import { buildPipeline } from "./lib";
import { DataSources } from "./app";

import { handleProposalCommand } from "./pipelines/proposal/command-handler";
import { handleProposalEvent } from "./pipelines/proposal/event-handler";

import { handleUserCommand } from "./pipelines/user/command-handler";
import { handleUserEvent } from "./pipelines/user/event-handler";

// Meaningless data sources
const dataSources: DataSources = {
  proposals: {
    async findById(id: string) {
      return null;
    }
  },
  users: {
    async findById(id: string) {
      return null;
    }
  }
};

const proposalPipeline = buildPipeline(
  handleProposalCommand,
  handleProposalEvent,
  dataSources
);

const userPipeline = buildPipeline(
  handleUserCommand,
  handleUserEvent,
  dataSources
);

// Doesn't do anyting -- just to check that it compiles
async function tryItOut() {
  // This corresponds to user id=321 attempting to accept proposal id=123
  const result = await proposalPipeline(
    { type: "ACCEPT_PROPOSAL", proposalId: "123" },
    { userId: "321" }
  );
}
