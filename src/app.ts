import { ProposalDataSource } from "./dataSources/proposals";
import { UserDataSource } from "./dataSources/users";

export interface DataSources {
  readonly proposals: ProposalDataSource;
  readonly users: UserDataSource;
}

export interface CommandContext {
  readonly userId: string;
}
