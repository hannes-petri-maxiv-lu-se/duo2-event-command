interface AcceptProposalCommand {
  type: "ACCEPT_PROPOSAL";
  proposalId: string;
}

interface RejectProposalCommand {
  type: "REJECT_PROPOSAL";
  proposalId: string;
}

export type ProposalCommand = AcceptProposalCommand | RejectProposalCommand;
