import { ProposalEvent } from "./events";
import { AsyncEventHandlerResult } from "../../lib";

async function executeQuery(sql: string, vars: Array<any>) {}

export async function handleProposalEvent(
  event: ProposalEvent
): AsyncEventHandlerResult {
  switch (event.type) {
    case "PROPOSAL_ACCEPTED": {
      const { proposalId } = event;
      await executeQuery(
        "UPDATE proposals SET status = 'accepted' WHERE id = ?",
        [proposalId]
      );
      return { id: proposalId, success: true };
    }

    default:
      return undefined;
  }
}
