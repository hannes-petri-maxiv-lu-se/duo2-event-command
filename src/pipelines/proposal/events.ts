interface ProposalAcceptedEvent {
  type: "PROPOSAL_ACCEPTED";
  proposalId: string;
}

interface ProposalRejectedEvent {
  type: "PROPOSAL_REJECTED";
  proposalId: string;
}

export type ProposalEvent = ProposalAcceptedEvent | ProposalRejectedEvent;
