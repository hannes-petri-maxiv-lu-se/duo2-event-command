import { isManagerAtBeamline } from "../../models/user";
import { isPending } from "../../models/proposal";
import {
  commandRejection,
  AsyncCommandHandlerResult
} from "../../lib";

import { ProposalEvent } from "./events";
import { ProposalCommand } from "./commands";
import { DataSources, CommandContext } from "../../app";

export async function handleProposalCommand(
  command: ProposalCommand,
  context: CommandContext,
  sources: DataSources
): AsyncCommandHandlerResult<ProposalEvent> {
  switch (command.type) {
    case "ACCEPT_PROPOSAL": {
      const { proposalId } = command;
      const user = await sources.users.findById(context.userId);
      const proposal = await sources.proposals.findById(proposalId);

      if (user == null) {
        return commandRejection("USER_NOT_FOUND");
      }

      if (proposal == null) {
        return commandRejection("PROPOSAL_NOT_FOUND");
      }

      if (!isManagerAtBeamline(user, proposal.beamlineId)) {
        return commandRejection("NOT_BEAMLINE_MANAGER");
      }

      if (!isPending(proposal)) {
        return commandRejection("NOT_PENDING");
      }

      return {
        type: "PROPOSAL_ACCEPTED",
        proposalId
      };
    }

    case "REJECT_PROPOSAL": {
      const { proposalId } = command;
      const user = await sources.users.findById(context.userId);
      const proposal = await sources.proposals.findById(proposalId);

      if (user == null) {
        return commandRejection("USER_NOT_FOUND");
      }

      if (proposal == null) {
        return commandRejection("PROPOSAL_NOT_FOUND");
      }

      if (!isManagerAtBeamline(user, proposal.beamlineId)) {
        return commandRejection("NOT_BEAMLINE_MANAGER");
      }

      if (!isPending(proposal)) {
        return commandRejection("NOT_PENDING");
      }

      return {
        type: "PROPOSAL_REJECTED",
        proposalId
      };
    }
  }
}
