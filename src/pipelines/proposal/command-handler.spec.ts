import { handleProposalCommand } from "./command-handler";
import { ProposalCommand } from "./commands";

import { ProposalDataSource } from "../../dataSources/proposals";
import { UserDataSource } from "../../dataSources/users";

import { Proposal } from "../../models/proposal";
import { User } from "../../models/user";

import { DataSources, CommandContext } from "../../app";

function proposalsSource(...proposals: Proposal[]): ProposalDataSource {
  return {
    async findById(id: string) {
      return proposals.find(proposal => proposal.id === id) || null;
    }
  };
}

function usersSource(...users: User[]): UserDataSource {
  return {
    async findById(id: string) {
      return users.find(user => user.id === id) || null;
    }
  };
}

const baseProposal = {
  id: "propsosal_id",
  mainProposerId: "main_proposer_id",
  principalInvestigatorId: "principal_investigator_id",
  coProposerIds: ["co_proposer_id_1", "co_proposer_id_2"],
  beamlineId: "beamline_id"
};

const user: User = {
  id: "user_id",
  roles: [],
  personalInformation: {
    firstName: "user_first_name",
    lastName: "user_last_name",
    affiliation: "user_affiliation"
  }
};

const beamlineManager1: User = {
  id: "beamline_manager1",
  personalInformation: {
    firstName: "beamline_manager1_first_name",
    lastName: "beamline_manager1_name1",
    affiliation: "beamline_manager1_affiliation"
  },
  roles: [{ type: "BEAMLINE_MANAGER", target: "beamline1" }]
};

const beamlineManager2: User = {
  id: "beamline_manager2",
  personalInformation: {
    firstName: "beamline_manager2_first_name",
    lastName: "beamline_manager2_last_name",
    affiliation: "beamline_manager2_affiliation"
  },
  roles: [{ type: "BEAMLINE_MANAGER", target: "beamline2" }]
};

describe("ACCEPT_PROPOSAL", async () => {
  const command: ProposalCommand = {
    type: "ACCEPT_PROPOSAL",
    proposalId: "proposal"
  };

  const approvedProposal: Proposal = {
    ...baseProposal,
    id: "proposal1",
    status: "APPROVED"
  };

  const rejectedProposal: Proposal = {
    ...baseProposal,
    id: "proposal2",
    status: "REJECTED"
  };

  const pendingProposal: Proposal = {
    ...baseProposal,
    id: "proposal3",
    status: "PENDING"
  };

  const dataSources: DataSources = {
    proposals: proposalsSource(
      approvedProposal,
      rejectedProposal,
      pendingProposal
    ),
    users: usersSource(user, beamlineManager1, beamlineManager2)
  };

  test("cannot accept already rejected proposal", () => {
    const context: CommandContext = {
      userId: "beamline_manager1"
    };

    const event = handleProposalCommand(command, context, dataSources);
    expect(event).toHaveProperty("type", "COMMAND_REJECTION");
  });

  test("cannot reject already accepted proposal", () => {
    const context: CommandContext = {
      userId: "beamline_manager1"
    };

    const event = handleProposalCommand(command, context, dataSources);
    expect(event).toHaveProperty("type", "COMMAND_REJECTION");
  });

  test("beamline manager of the same beamline can approve proposal", () => {
    const context: CommandContext = {
      userId: "beamline_manager1"
    };

    const event = handleProposalCommand(command, context, dataSources);
    expect(event).toHaveProperty("type", "PROPOSAL_APPROVED");
  });

  test("beamline manager of another beamline cannot approve proposal", () => {
    const context: CommandContext = {
      userId: "beamline_manager2"
    };

    const event = handleProposalCommand(command, context, dataSources);
    expect(event).toHaveProperty("type", "COMMAND_REJECTION");
  });
});
