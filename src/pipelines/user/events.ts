import { PersonalInformation } from "../../models/user";

interface PersonalInformationUpdatedEvent {
  type: "PERSONAL_INFORMATION_UPDATED";
  userId: string;
  personalInformation: PersonalInformation;
}

export type UserEvent = PersonalInformationUpdatedEvent;
