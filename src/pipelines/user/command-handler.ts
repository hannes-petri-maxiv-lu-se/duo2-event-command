import { CommandContext, DataSources } from "../../app";
import { AsyncCommandHandlerResult, commandRejection } from "../../lib";

import { UserCommand } from "./commands";
import { UserEvent } from "./events";

import { isValidPersonalInformation } from "../../models/user";

export async function handleUserCommand(
  command: UserCommand,
  context: CommandContext,
  _sources: DataSources
): AsyncCommandHandlerResult<UserEvent> {
  switch (command.type) {
    case "UPDATE_PERSONAL_INFORMATION": {
      const { personalInformation } = command;

      if (isValidPersonalInformation(personalInformation)) {
        return {
          type: "PERSONAL_INFORMATION_UPDATED",
          userId: context.userId,
          personalInformation
        };
      } else {
        return commandRejection("INVALID_PERSONAL_INFORMATION");
      }
    }
  }
}
