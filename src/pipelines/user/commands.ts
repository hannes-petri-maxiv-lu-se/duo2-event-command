import { PersonalInformation } from "../../models/user";

interface UpdatePersonalInformationCommand {
  type: "UPDATE_PERSONAL_INFORMATION";
  personalInformation: PersonalInformation;
}

export type UserCommand = UpdatePersonalInformationCommand;
