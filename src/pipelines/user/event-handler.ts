import { UserEvent } from "./events";
import { AsyncEventHandlerResult } from "../../lib";

async function executeQuery(sql: string, vars: Array<any>) {}

export async function handleUserEvent(
  event: UserEvent
): AsyncEventHandlerResult {
  switch (event.type) {
    case "PERSONAL_INFORMATION_UPDATED": {
      const { userId, personalInformation } = event;
      const { firstName, lastName, affiliation } = personalInformation;

      await executeQuery(
        `UPDATE users
        SET first_name = ?,
          last_name = ?,
          affiliation = ?
        WHERE id = ?`,
        [firstName, lastName, affiliation, userId]
      );

      return { id: userId, success: true };
    }

    default:
      return undefined;
  }
}
