export interface Proposal {
  id: string;
  mainProposerId: string;
  principalInvestigatorId: string;
  coProposerIds: string[];
  beamlineId: string;
  status: "PENDING" | "APPROVED" | "REJECTED";
}

export function isPending(proposal: Proposal) {
  return proposal.status === "PENDING";
}
