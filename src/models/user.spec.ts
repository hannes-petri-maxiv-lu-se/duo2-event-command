import {
  User,
  UserRole,
  PersonalInformation,
  isManagerAtBeamline
} from "./user";

const emptyPersonalInformation: PersonalInformation = {
  firstName: "",
  lastName: "",
  affiliation: ""
};

test("beamline manager", () => {
  const role: UserRole = {
    type: "BEAMLINE_MANAGER",
    target: "some beamline"
  };

  const user1: User = {
    id: "",
    roles: [role],
    personalInformation: emptyPersonalInformation
  };

  const user2: User = {
    id: "",
    roles: [],
    personalInformation: emptyPersonalInformation
  };

  expect(isManagerAtBeamline(user1, "some beamline")).toBe(true);
  expect(isManagerAtBeamline(user1, "some other beamline")).toBe(false);
  expect(isManagerAtBeamline(user2, "some beamline")).toBe(false);
});
