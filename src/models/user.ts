type RoleType = "BEAMLINE_MANAGER" | "USER_OFFICER";

export interface PersonalInformation {
  readonly firstName: string;
  readonly lastName: string;
  readonly affiliation: string;
}

export interface UserRole {
  readonly type: RoleType;
  readonly target?: string;
}

export interface User {
  readonly id: string;
  readonly roles: UserRole[];
  readonly personalInformation: PersonalInformation;
}

export function isValidPersonalInformation(
  personalInformation: PersonalInformation
): boolean {
  const { firstName, lastName, affiliation } = personalInformation;

  if (firstName === "") {
    return false;
  }

  if (lastName === "") {
    return false;
  }

  if (affiliation === "") {
    return false;
  }

  return true;
}

export function isManagerAtBeamline(user: User, beamlineId: string) {
  return user.roles.some(
    role => role.type === "BEAMLINE_MANAGER" && role.target === beamlineId
  );
}
